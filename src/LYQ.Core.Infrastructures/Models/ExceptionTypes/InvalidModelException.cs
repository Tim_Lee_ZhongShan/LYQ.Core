﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Models.ExceptionTypes
{
    public class InvalidModelException : Exception
    {
        public InvalidModelException() : base() { }

        public InvalidModelException(string message) : base(message) { }

        public InvalidModelException(string message, Exception exception) : base(message, exception) { }

    }
}
