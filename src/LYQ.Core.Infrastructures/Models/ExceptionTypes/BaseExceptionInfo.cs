﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Models.ExceptionTypes
{
    public class BaseExceptionInfo
    {
        public static string BaseMessage(Type type, string propName, string value, string errorMessage)
        {
            return string.Format("Class:{0},Properties: {1},Value: {2},Exception: {3}", type.ToString(), propName, value, errorMessage);
        }

    }
}
