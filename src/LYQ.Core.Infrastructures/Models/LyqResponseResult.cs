﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Models
{
    public class LyqResponseResult
    {
        public string TimeStamp { get; set; }

        public bool Success { get; set; }

        public Object Result { get; set; }

        public string Error { get; set; }

    }
}
