﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Models.Authentication
{
    public class LoginResult
    {
        public LoginResult() { }

        public LoginResult(bool success, string tokenString) { this.Success = success; this.TokenString = tokenString; }

        public bool Success { get; set; }

        public string Message { get; set; }

        public string TokenString { get; set; }


    }
}
