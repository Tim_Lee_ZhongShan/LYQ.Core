﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Utils.Models
{
    public partial class UnmanagedDispose: IDisposable
    {
        private bool isDisposed = false;
        public UnmanagedDispose() { }

        /// <summary>
        /// 析构函数
        /// </summary>
        ~UnmanagedDispose()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!this.isDisposed)
            {
                if (isDisposed) { }
            }

            this.isDisposed = true;
        }
    }
}
