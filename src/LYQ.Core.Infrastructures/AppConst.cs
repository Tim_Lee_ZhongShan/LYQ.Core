﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures
{
    public class AppConst
    {
        /// <summary>
        /// 数据库访问超时时间,单位s
        /// </summary>
        public const int CommandTimeOut = 900;

        /// <summary>
        /// 项目前缀
        /// </summary>
        public const string ProjectPrefix = "LYQ.Core";

        /// <summary>
        /// Web程序集
        /// </summary>
        public const string WebAssembly = "LYQ.Core.Web";

        /// <summary>
        /// Businesses程序集
        /// </summary>
        public const string BusinessesAssembly = "LYQ.Core.Businesses";

        /// <summary>
        /// 基础设施程序集
        /// </summary>
        public const string InfrastructuresAssembly = "LYQ.Core.Infrastructures";

        /// <summary>
        /// Repositories程序集
        /// </summary>
        public const string RepositoriesAssembly = "LYQ.Core.Repositories";
        


    }
}
