﻿
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Extras.DynamicProxy;
using AutoMapper;
using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using LYQ.Core.Infrastructures.Cores.Injections.Interceptors;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;


namespace LYQ.Core.Infrastructures
{


    public class Initialize
    {
        /// <summary>
        /// 获取当前程序集所引用了的程序集
        /// </summary>
        public static Assembly[] ReferenceAssembly =>
             Assembly.GetEntryAssembly()
                .GetReferencedAssemblies()
                .Where(ass => ass.Name.StartsWith(AppConst.ProjectPrefix))
                .Select(Assembly.Load)
                .ToArray();

        /// <summary>
        /// 注册所有继承了Autofac的Module模块
        /// </summary>
        /// <param name="builder"></param>
        public static void Configure(ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(typeof(Initialize).Assembly);  // 注册当前程序集所有引用的所有模块                      
        }

        /// <summary>
        /// 注册AutoMapper
        /// </summary>
        /// <param name="services"></param>
        public static void Configure(IServiceCollection services)
        {
            Assembly bussinessesAssembly = Assembly.Load(AppConst.BusinessesAssembly); // Entity <=> Dtp

            Assembly webAssembly = Assembly.Load(AppConst.WebAssembly); // Dto <=> ViewModel(parameters)

            services.AddAutoMapper(bussinessesAssembly, webAssembly);


        }

    }


}
