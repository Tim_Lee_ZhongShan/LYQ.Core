﻿using Autofac;
using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Cores.Injections.Modules
{
    public class InterceptorsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Initialize.ReferenceAssembly).PublicOnly()
               .Where(type => !type.IsAbstract && typeof(IInterceptor).IsAssignableFrom(type));    // 注册所有实现IInterceptor接口的拦截器
        }
    }
}
