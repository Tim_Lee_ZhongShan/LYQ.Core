﻿using Autofac;
using LYQ.Core.Infrastructures.Cores.DataStore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace LYQ.Core.Infrastructures.Cores.Injections.Modules
{
    public class DataAccessorModule : Module    
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DataAccessor>().As<IDataAccessor>().InstancePerLifetimeScope();//往IDataAccessor中注入DataAccessor            
        }
    }
}
