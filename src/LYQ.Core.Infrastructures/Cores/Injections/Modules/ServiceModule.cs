﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using LYQ.Core.Infrastructures.Cores.Injections.Interceptors;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Cores.Injections.Modules
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Initialize.ReferenceAssembly).PublicOnly()
                .Where(type => !type.IsAbstract && typeof(IService).IsAssignableFrom(type))
                .AsImplementedInterfaces()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(TransactionInterceptor))
                .InstancePerLifetimeScope();//如果继承了多个，选择第一个
        }
    }
}
