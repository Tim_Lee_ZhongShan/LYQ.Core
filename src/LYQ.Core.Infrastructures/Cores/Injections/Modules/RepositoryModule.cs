﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using LYQ.Core.Infrastructures.Cores.Injections.Interceptors;

namespace LYQ.Core.Infrastructures.Cores.Injections.Modules
{
    public class RepositoryModule : Module

    {
        protected override void Load(ContainerBuilder builder)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.Load(AppConst.RepositoriesAssembly);

            builder.RegisterAssemblyTypes(assembly).PublicOnly()
                .Where(type => !type.IsAbstract && typeof(IRepository).IsAssignableFrom(type))
                .AsImplementedInterfaces()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(TransactionInterceptor))
                .InstancePerLifetimeScope();//如果继承了多个，选择第一个
        }
    }
}
