﻿using Castle.DynamicProxy;
using LYQ.Core.Infrastructures.Cores.DataStore;
using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using LYQ.Core.Infrastructures.Cores.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace LYQ.Core.Infrastructures.Cores.Injections.Interceptors
{
    public class TransactionInterceptor : IInterceptor
    {
        // 注入IConfiguration 配置文件操作对象，用户读取 .json 文件数据
        private readonly IConfiguration _configuration;
        // 注入IDataAccessor 数据库访问器对象，数据库操作封装在其属性中
        private readonly IDataAccessor _dataAccessor;

        public TransactionInterceptor(IConfiguration configuration, IDataAccessor dataAccessor)
        {
            _configuration = configuration;
            _dataAccessor = dataAccessor;
        }

        public IsolationLevel IsolationLevel { get; set; } = IsolationLevel.ReadCommitted;

        public void Intercept(IInvocation invocation)
        {
            // 如果数据库已经开启，直接跳过并继续执行方法
            if (!string.IsNullOrWhiteSpace(_dataAccessor.ConnectingLabel))
            {
                invocation.Proceed();
                return;
            }

            // 获取当前执行的方法对象
            var method = invocation.MethodInvocationTarget ?? invocation.Method;
            // 获取当前方法所在的类类型
            var reflectedType = method.ReflectedType;

            // 如果是抽象类或者没有继承接口IService和IRepository，实现类直接跳过并继续执行方法
            if (reflectedType.IsAbstract
                || (!typeof(IService).IsAssignableFrom(reflectedType) && !typeof(IRepository).IsAssignableFrom(reflectedType)))
            {
                invocation.Proceed();   // 继续执行
                return;
            }

            // 获取方法是否贴有 [Transaction] 日志特性
            var transactionAttribute = method.GetCustomAttributes(typeof(TransactionAttribute), true).FirstOrDefault() as TransactionAttribute;

            // 如果服务层或仓储层方法贴有 [NonConnection] 特性，或者服务层未贴有 [Transaction] 特性直接跳过并继续执行方法
            if (method.GetCustomAttributes(typeof(NonConnectionAttribute), true).FirstOrDefault() is NonConnectionAttribute
                || (transactionAttribute == null && typeof(IService).IsAssignableFrom(reflectedType)))
            {
                invocation.Proceed();   // 继续执行
                return;
            }

            try
            {
                // 如果数据库操作对象为空
                if (_dataAccessor.DbContext == null)
                {
                    var sqlConnection = new SqlConnection();
                    sqlConnection.ConnectionString = _configuration["ConnectionStrings:Default"];

                    _dataAccessor.DbContext = sqlConnection;  // 将注入的数据库操作对象赋值给数据访问器DbContext属性
                    _dataAccessor.ConnectingLabel = $"{reflectedType.FullName}.{method.Name}";   // 记录当前开启数据库连接的方法全名称
                }

                //如果 贴有[Transaction] 特性，则开启事务
                if (transactionAttribute != null)
                {
                    //开启事务
                    _dataAccessor.DbTransaction = _dataAccessor.DbContext.BeginTransaction(this.IsolationLevel);
                    invocation.Proceed();   // 继续执行
                    // 提交的事务
                    _dataAccessor.DbTransaction.Commit();
                }
                else
                {
                    invocation.Proceed();   // 继续执行
                }
            }
            catch (Exception)
            {
                // 如果执行出现异常，则回滚事务
                if (transactionAttribute != null) _dataAccessor.DbTransaction.Rollback();
                throw;   // 抛出异常
            }
            finally
            {
                _dataAccessor.ConnectingLabel = null;    // 清空记录点
                _dataAccessor.DbContext.Close();    // 关闭数据库连接
            }

        }
    }
}
