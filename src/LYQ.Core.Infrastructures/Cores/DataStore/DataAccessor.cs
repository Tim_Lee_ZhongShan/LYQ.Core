﻿
using LYQ.Core.Infrastructures.Utils.Models;
using System.Data;

namespace LYQ.Core.Infrastructures.Cores.DataStore
{
    public class DataAccessor : UnmanagedDispose, IDataAccessor
    {
        /// <summary>
        /// 动态的判断数据库的连接状况
        /// </summary>
        public string ConnectingLabel { get; set; }

        public IDbConnection DbContext { get; set; }

        /// <summary>
        /// 数据库事务
        /// </summary>
        public IDbTransaction DbTransaction { get; set; }

        /// <summary>
        /// 访问超时时间
        /// </summary>
        public int CommandTimeout { get; set; } = AppConst.CommandTimeOut;

        protected override void Dispose(bool isDisposing)
        {
            if (!isDisposing) return;   // 如果已经释放，结束执行
            if (DbContext == null) return;  // 如果数据库连接对象为空，结束执行
            if (DbContext.State != ConnectionState.Open) return; // 如果数据库连接对象非打开状况，结束执行
            DbContext.Close();  // 关闭数据库连接
            DbContext.Dispose();    // 释放内存
        }
    }
}
