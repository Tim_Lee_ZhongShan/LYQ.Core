﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace LYQ.Core.Infrastructures.Cores.DataStore
{
    public partial interface IDataAccessor : IDisposable
    {
        IDbConnection DbContext { get; set; }

        IDbTransaction DbTransaction { get; set; }

        int CommandTimeout { get; set; }

        /// <summary>
        /// 数据库连接起始点，标记作用，智能判断何时开启/关闭数据库连接
        /// </summary>
        string ConnectingLabel { get; set; }

    }
}
