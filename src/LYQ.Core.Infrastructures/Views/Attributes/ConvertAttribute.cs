﻿using LYQ.Core.Infrastructures.Views.Models.Search;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Views.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ConvertAttribute : Attribute
    {
        /// <summary>
        /// 匹配规则
        /// </summary>
        public WhereSetting WhereSetting { get; set; }

        /// <summary>
        /// 匹配字段，以","结尾，最后一个不带","
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 前缀,默认不写
        /// </summary>
        public string Prefix { get; set; } = string.Empty;

        /// <summary>
        /// 自定义函数,默认不写
        /// </summary>
        public string Invoke { get; set; } = string.Empty;

        /// <summary>
        /// 类型，默认string
        /// </summary>
        public Type ValueType { get; set; } = typeof(string);

    }
}
