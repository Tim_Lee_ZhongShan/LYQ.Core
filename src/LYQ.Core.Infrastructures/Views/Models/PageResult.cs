﻿using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Views.Models
{
    public class PageResult<T> where T : class, new()
    {       
        /// <summary>
        /// 总记录数
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 返回数据
        /// </summary>
        public IEnumerable<T> PageData { get; set; }

    }
}
