﻿using LYQ.Core.Infrastructures.Models.ExceptionTypes;
using LYQ.Core.Infrastructures.Views.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LYQ.Core.Infrastructures.Views.Models.Search
{
    public static class SearcherExtension
    {
        public static (string, IDictionary<string, object>) Convert(this Searcher searcher)
        {
            StringBuilder where = new StringBuilder();
            Dictionary<string, object> paras = new Dictionary<string, object>();

            //获取searcher的modelType
            var modelType = searcher.GetType();
            //获取searcher的所有属性
            var props = modelType.GetProperties();

            for (int i = 0; i < props.Length; i++)
            {
                //如果searcher的属性上贴有ConvertAttribute的特性
                if (props[i].GetCustomAttributes(typeof(ConvertAttribute), true).FirstOrDefault() is ConvertAttribute convertAttribute)
                {
                    var propNam = props[i].Name;
                    var value = System.Convert.ChangeType(props[i].GetValue(searcher), convertAttribute.ValueType);

                    string[] fields = (convertAttribute.Fields ?? propNam + ",").Split(",", StringSplitOptions.RemoveEmptyEntries);
                    var isFirstJoin = i == 0;

                    switch (convertAttribute.WhereSetting)
                    {
                        case WhereSetting.Equal:
                            where.Append(BuildWhereCondition(fields, convertAttribute.WhereSetting, propNam, convertAttribute.Prefix, isFirstJoin));
                            paras.Add(propNam, value);
                            break;
                        case WhereSetting.RightLike:
                            where.Append(BuildWhereCondition(fields, convertAttribute.WhereSetting, propNam, convertAttribute.Prefix, isFirstJoin));
                            paras.Add(propNam, value + "%");
                            break;
                        case WhereSetting.LeftLike:
                            where.Append(BuildWhereCondition(fields, convertAttribute.WhereSetting, propNam, convertAttribute.Prefix, isFirstJoin));
                            paras.Add(propNam, "%" + value);
                            break;
                        case WhereSetting.Like:
                            where.Append(BuildWhereCondition(fields, convertAttribute.WhereSetting, propNam, convertAttribute.Prefix, isFirstJoin));
                            paras.Add(propNam, "%" + value + "%");
                            break;
                        case WhereSetting.In:
                            where.Append(BuildWhereCondition(fields, convertAttribute.WhereSetting, propNam, convertAttribute.Prefix, isFirstJoin));
                            paras.Add(propNam, value);
                            break;
                        case WhereSetting.NotIn:
                            where.Append(BuildWhereCondition(fields, convertAttribute.WhereSetting, propNam, convertAttribute.Prefix, isFirstJoin));
                            paras.Add(propNam, value);
                            break;
                        case WhereSetting.Customize:
                            var methodName = convertAttribute.Invoke;
                            if (!string.IsNullOrWhiteSpace(methodName))
                            {
                                var method = modelType.GetMethod(methodName);
                                if (method != null)
                                {
                                    var whereObj = (ValueTuple<string, Dictionary<string, object>>)(method.Invoke(searcher, null));
                                    if (!string.IsNullOrWhiteSpace(whereObj.Item1))
                                        where.Append(whereObj.Item1);

                                    if (whereObj.Item2 != null)
                                    {
                                        foreach (var item in whereObj.Item2)
                                        {
                                            paras.Add(item.Key, item.Value);
                                        }
                                    }

                                }
                            }
                            break;
                        default:
                            break;


                    }
                }
                else
                {
                    //判断order_cloumn 和 order_type 是否合法有效
                    if (props[i].PropertyType == typeof(string) && props[i].GetValue(searcher) != null) CheckSpecialChar(searcher, props[i].Name, props[i].GetValue(searcher).ToString().Trim());
                }
            }

            return (where.ToString(), paras);
        }

        /// <summary>
        /// 判断Order_Column和Order_Type有没有特殊的字符串
        /// </summary>
        /// <param name="searcher"></param>
        /// <param name="propName"></param>
        /// <param name="value"></param>
        private static void CheckSpecialChar(this Searcher searcher, string propName, string value)
        {
            const string ORDER_COLUMN = "ORDERCOLUMN";
            const string ORDER_TYPE = "ORDERTYPE";
            const string ASC = "ASC";
            const string DESC = "DESC";

            if (string.IsNullOrWhiteSpace(value)) return;

            if (propName.ToUpper() == ORDER_COLUMN)
            {
                var reg = new Regex("^[0-9a-zA-Z_]+$", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace);

                if (!reg.IsMatch(value))
                    throw new InvalidModelException(BaseExceptionInfo.BaseMessage(searcher.GetType(), propName, value, "Invalid char"));

            }

            if (propName.ToUpper() == ORDER_TYPE)
            {
                if (value.ToUpper() != ASC && value.ToUpper() != DESC)
                    throw new InvalidModelException(BaseExceptionInfo.BaseMessage(searcher.GetType(), propName, value, "Invalid char"));
            }

        }

        /// <summary>
        /// 创建where语句
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="whereSetting"></param>
        /// <param name="propName"></param>
        /// <param name="prefix"></param>
        /// <param name="isFirstJoin"></param>
        /// <returns></returns>
        private static string BuildWhereCondition(string[] fields, WhereSetting whereSetting, string propName, string prefix, bool isFirstJoin = false)
        {
            StringBuilder where = new StringBuilder();
            prefix = string.IsNullOrWhiteSpace(prefix) ? "" : prefix + ".";

            for (int i = 0; i < fields.Length; i++)
            {
                var or = propName + " or ";
                if (i == fields.Length - 1) or = propName + " ";

                switch (whereSetting)
                {
                    case WhereSetting.RightLike:
                    case WhereSetting.LeftLike:
                    case WhereSetting.Like:
                        where.Append($" {prefix}{fields[i]} Like @{or} ");
                        break;
                    case WhereSetting.Equal:
                        where.Append($"{prefix}{fields[i]} = {or}");
                        break;
                    case WhereSetting.In:
                        where.Append($"{prefix}{fields[i]} in {or}");
                        break;
                    case WhereSetting.NotIn:
                        where.Append($"{prefix}{fields[i]} not in {or}");
                        break;
                    default:
                        break;

                }
            }

            return $"{(isFirstJoin ? " " : " and ")} {(string.IsNullOrWhiteSpace(where.ToString()) ? " " : " ( " + where.ToString().Trim() + " ) ")}  ";
        }


    }
}
