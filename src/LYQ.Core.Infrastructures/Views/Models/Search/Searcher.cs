﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Views.Models.Search
{
    public abstract class Searcher
    {
    }

    public enum WhereSetting
    {
        Equal,
        Like,
        LeftLike,
        RightLike,
        In,
        NotIn,
        Customize
    }

}
