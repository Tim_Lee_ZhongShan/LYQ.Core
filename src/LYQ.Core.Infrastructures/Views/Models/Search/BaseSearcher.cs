﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Infrastructures.Views.Models.Search
{
    public class BaseSearcher : Searcher
    {
        public virtual int PageIndex { get; set; } = 1;

        public virtual int PageSize { get; set; } = 10;

        public virtual string OrderColumn { get; set; }

        public virtual string OrderType { get; set; }

    }
}
