﻿using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Models.User
{
    public class UserEntity : BaseEntity<long>
    {      
        public string Name { get; set; }

        public string Email { get; set; }

        public string Gender { get; set; }

        public int Age { get; set; }

        //public string Password { get; set; }

        public bool IsActive { get; set; }

    }
}
