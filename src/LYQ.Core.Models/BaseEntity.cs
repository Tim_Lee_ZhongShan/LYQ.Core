﻿using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using System;

namespace LYQ.Core.Models
{
    public class BaseEntity<TKek> : IEntity where TKek : new()
    {
        public virtual TKek Id { get; set; }

        public virtual string CreatedBy { get; set; }

        public virtual DateTime? CreatedDate { get; set; }

        public virtual string UpdatedBy { get; set; }

        public virtual DateTime? UpdatedDate { get; set; }
    }
}
