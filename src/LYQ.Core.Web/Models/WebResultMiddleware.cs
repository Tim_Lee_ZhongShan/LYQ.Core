﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;

namespace LYQ.Core.Web.Models
{
    public class WebResultMiddleware : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            //if (context.HttpContext.Request.Path.HasValue)
            //{
            //    if (context.HttpContext.Request.Path.Value.ToLower().IndexOf(".inside.") < 0)
            //    {
            //        if (context.Result is FileContentResult || context.Result is EmptyResult)
            //        {
            //            return;
            //        }

            //        if (context.Result is ObjectResult)
            //        {
            //            var objectResult = context.Result as ObjectResult;

            //            var settings = new JsonSerializerSettings()
            //            {
            //                ContractResolver = new NullToEmptyStringResolver(),
            //                DateFormatString = "yyyy-MM-dd HH:mm:ss"
            //            };

            //            context.Result = new JsonResult(new { data = objectResult.Value }, settings);
            //        }
            //        else
            //        {
            //            context.Result = new ObjectResult(new { data = new { } });
            //        }

            //    }
            //}

            //根据实际需求进行具体实现
            if (context.Result is ObjectResult)
            {
                var objectResult = context.Result as ObjectResult;
                if (objectResult.Value == null)
                {
                    context.Result = new ObjectResult(new { code = 404, sub_msg = "未找到资源", msg = "" });
                }
                else
                {
                    context.Result = new ObjectResult(new { code = 200, msg = "", result = objectResult.Value });
                }
            }
            else if (context.Result is EmptyResult)
            {
                context.Result = new ObjectResult(new { code = 404, sub_msg = "未找到资源", msg = "" });
            }
            else if (context.Result is ContentResult)
            {
                context.Result = new ObjectResult(new { code = 200, msg = "", result = (context.Result as ContentResult).Content });
            }
            else if (context.Result is StatusCodeResult)
            {
                context.Result = new ObjectResult(new { code = (context.Result as StatusCodeResult).StatusCode, sub_msg = "", msg = "" });
            }

        }


        public class NullToEmptyStringResolver : Newtonsoft.Json.Serialization.DefaultContractResolver
        {
            protected override IList<Newtonsoft.Json.Serialization.JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                return type.GetProperties().Select(p =>
                {
                    var jp = base.CreateProperty(p, memberSerialization);
                    jp.ValueProvider = new NullToEmptyStringValueProvider(p);
                    return jp;
                }).ToList();
            }
        }

        public class NullToEmptyStringValueProvider : IValueProvider
        {
            PropertyInfo _MemberInfo;
            public NullToEmptyStringValueProvider(PropertyInfo memberInfo)
            {
                _MemberInfo = memberInfo;
            }

            public object GetValue(object target)
            {
                object result = _MemberInfo.GetValue(target);
                if (result == null) result = "";
                return result;

            }

            public void SetValue(object target, object value)
            {
                _MemberInfo.SetValue(target, value);
            }
        }
    }





}
