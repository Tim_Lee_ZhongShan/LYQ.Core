﻿using LYQ.Core.Infrastructures.Views.Attributes;
using LYQ.Core.Infrastructures.Views.Models.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LYQ.Core.Web.Areas.Maintance.Models
{
    public class UserSearcher : BaseSearcher
    {
        [Convert(Fields = "name", WhereSetting = WhereSetting.Like)]
        public string Name { get; set; }

        [Convert(Fields = "email", WhereSetting = WhereSetting.Like)]
        public string Email { get; set; }

    }
}
