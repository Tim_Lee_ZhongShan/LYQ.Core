﻿using System.Collections.Generic;
using LYQ.Core.Businesses.User;
using LYQ.Core.Businesses.User.Dto;
using LYQ.Core.Infrastructures.Views.Models;
using LYQ.Core.Infrastructures.Views.Models.Search;
using LYQ.Core.Web.Areas.Maintance.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LYQ.Core.Web.Areas.Maintance.Controllers
{
    /// <summary>
    /// 用户
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// 获取所有的用户
        /// </summary>
        /// <returns></returns>        
        [HttpGet]        
        [Authorize]
        public IList<UserDto> GetAll()
        {
            return _userService.GetAllUser();
        }

        /// <summary>
        /// 分页获取用户
        /// </summary>
        /// <param name="searcher"></param>
        /// <returns></returns>        
        [HttpGet]
        public PageResult<UserDto> GetPageList([FromQuery]UserSearcher searcher)
        {
            var (whereCondition, paras) = searcher.Convert();

            var result = new PageResult<UserDto>();

            result.TotalCount = 10;
            result.PageData = null;

            return result;
        }
    }
}