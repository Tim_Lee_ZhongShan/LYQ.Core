using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;
using System.Text;
using Autofac;
using LYQ.Core.Businesses.User.Dto;
using LYQ.Core.Infrastructures;
using LYQ.Core.Web.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace LYQ.Core.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews(options =>
            {
                //options.Filters.Add(typeof(WebResultMiddleware));
                //options.RespectBrowserAcceptHeader = true;
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.DateFormatString = Configuration["DefaultDateFormat"];
            }).AddXmlDataContractSerializerFormatters();


            //注册dapper
            Initialize.Configure(services);


            //JwtBearer身份认证注册
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true, //验证Issuer
                    ValidateAudience = true, //验证Audience
                    ValidateIssuerSigningKey = true, //验证SecurityKey
                    ValidateLifetime = true, //验证失效时间


                    ValidIssuer = Configuration["Authentication:JwtBearer:Issuer"],
                    ValidAudience = Configuration["Authentication:JwtBearer:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Authentication:JwtBearer:SecretKey"]))
                };
            });


            //注册swagger
            services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "LYQ.Core API",
                    Version = "v1",
                    Description = "A sample demo for liyongqian",
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact() { Name = "liyongqian", Email = "1454759684@qq.com", Url = new Uri("https://blog.csdn.net/weixin_44308006") }
                });

                #region swagger使用xml注释 
                var xmlFile = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                option.IncludeXmlComments(xmlPath, true);
                #endregion

                #region swagger 身份认证
                option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "Please enter into field the word 'Bearer' followed by a space and the JWT value",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey

                });

                var openApiSecurityScheme = new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference() { Id = "Bearer", Type = ReferenceType.SecurityScheme }
                };

                option.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {openApiSecurityScheme,Array.Empty<string>() }

                });

                #endregion

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        context.Response.StatusCode = 500;
                        var dto = new UserDto() { Gender = "male", Id = 1 };
                        //context.Response.WriteAsync(dto);
                    });

                });
            }

            if (!env.IsDevelopment())//如果是开发环境，才启动http重定向
            {
                app.UseHttpsRedirection();
            }

            app.UseStaticFiles();//静态文件存入项目中

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "LYQ.Core API");
            });

            app.UseRouting();


            app.UseAuthentication();//身份认证
            app.UseAuthorization();//授权

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "API",
                    pattern: "api/{controller}/{action}/{id?}");

                endpoints.MapControllerRoute(
                    name: "area",
                    pattern: "/api/{area:exists}/{controller}/{action}/{id?}");

            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            //注册IServeice,IRepository,Interceptor接口依赖
            Initialize.Configure(builder);
        }


    }
}
