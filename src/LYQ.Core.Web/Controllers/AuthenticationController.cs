﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Logging;
using LYQ.Core.Infrastructures.Models.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace LYQ.Core.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public AuthenticationController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        [HttpPost]
        public async Task<LoginResult> Login([Required]string userName, [Required]string password)
        {
            var result = new LoginResult(false, "");

            if (userName.Equals("liyongqian", StringComparison.OrdinalIgnoreCase))
            {

                result.TokenString = GenerateTokenString(userName);


                result.Success = true;
                result.Message = "登录成功";
            }
            else
            {
                result.Message = "登录失败";
            }

            return result;
        }

        private string GenerateTokenString(string userName)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Authentication:JwtBearer:SecretKey"]));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256Signature);

            var tokenOptions = new JwtSecurityToken
                (
                  issuer: _configuration["Authentication:JwtBearer:Issuer"],
                  audience: _configuration["Authentication:JwtBearer:Audience"],
                  claims: GenerateTokenClaims(userName),
                  expires: DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["Authentication:JwtBearer:ExpireDate"])),
                  signingCredentials: signinCredentials
                );

            var tokenHandler = new JwtSecurityTokenHandler();

            return tokenHandler.WriteToken(tokenOptions);
        }

        private IList<Claim> GenerateTokenClaims(string userName)
        {
            var result = new List<Claim>();

            result.Add(new Claim("UserName", userName));
            //result.Add(new Claim("Roles", ""));
            //result.Add(new Claim("Function", ""));

            return result;
        }


    }
}