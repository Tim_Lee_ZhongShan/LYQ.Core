using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System.IO;

namespace LYQ.Core.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {


            var hostBuilder = CreateHostBuilder(args);
            var host = hostBuilder.Build();

            host.Run();//运行宿主机            
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            //创建宿主机builder
            var hostBuilder = Host.CreateDefaultBuilder(args);

            //使用AutoFac 代替微软的DI
            hostBuilder.UseServiceProviderFactory(new AutofacServiceProviderFactory());

            //配置Serilog
            hostBuilder.UseSerilog((context, logger) =>
            {
                logger.ReadFrom.Configuration(context.Configuration);
            });

            //默认使用Kestrel web服务器
            hostBuilder.ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
                    
            return hostBuilder;
        }
    }
}
