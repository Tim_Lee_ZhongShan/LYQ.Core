﻿using Dapper;
using LYQ.Core.Infrastructures.Cores.DataStore;
using LYQ.Core.Infrastructures.Views.Models;
using LYQ.Core.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Repositories.User
{
    public class UserRepository : BaseRepository<UserEntity>, IUserRepository
    {
        public UserRepository(IDataAccessor dataAccessor)
        {
            _dataAccessor = dataAccessor;
        }

        public IEnumerable<UserEntity> GetAllUser()
        {
            string sql = $@"
SELECT [id]
      ,[name]
      ,[email]
      ,[gender]
      ,[Age]
      ,[isActive]
FROM [dbo].[User]
";

            return base.GetAll(sql);
        }

        public PageResult<UserEntity> GetPageList(int pageIndex, int pageSize, string whereSql = null, object param = null, string orderBy = null)
        {
            string sql = "";

            return base.GetPageList(sql, param);
        }

        public UserEntity GetUserById<TKey>(TKey key) where TKey : class, new()
        {
            string sql = $@"
SELECT [id]
      ,[name]
      ,[email]
      ,[gender]
      ,[Age]
      ,[isActive]
FROM[dbo].[User]
Where name=@name and isActive=1
";

            return base.GetById(sql, new { name = key });
        }

        public int Insert(UserEntity userEntity)
        {
            string sql = "Insert into [dbo].[User] ([name],[email],[gender],[Age],[isActive]) VALUES (@Name,@Email,@Gender,@Age,@IsActive)";

            return base.Insert(sql, userEntity);
        }

        public int Update(UserEntity userEntity)
        {
            string sql = "Update [dbo].[User] set name=@Name,email=@Email,gender=@Gender,age=@Age,isActive=@IsActive Where id=@id";

            return base.Update(sql, userEntity);
        }
    }
}
