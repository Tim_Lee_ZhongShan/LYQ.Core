﻿using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using LYQ.Core.Infrastructures.Views.Models;
using LYQ.Core.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Repositories.User
{
    public interface IUserRepository : IRepository
    {
        IEnumerable<UserEntity> GetAllUser();

        UserEntity GetUserById<TKey>(TKey key) where TKey : class, new();

        int Insert(UserEntity userEntity);

        int Update(UserEntity userEntity);

        PageResult<UserEntity> GetPageList(int pageIndex, int pageSize, string whereSql = null, object param = null, string orderBy = null);

    }
}
