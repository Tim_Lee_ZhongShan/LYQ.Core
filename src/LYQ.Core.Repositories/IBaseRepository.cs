﻿using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using LYQ.Core.Infrastructures.Views.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;

namespace LYQ.Core.Repositories
{
    public interface IBaseRepository<TEntity> : IRepository where TEntity : class, IEntity, new()
    {
        IEnumerable<TEntity> GetAll(string sql, Object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text);

        TEntity GetById(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text);

        PageResult<TEntity> GetPageList(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text);

        int Insert(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text);

        int Update(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text);      
    }
}
