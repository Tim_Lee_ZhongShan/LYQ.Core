﻿using Dapper;
using LYQ.Core.Infrastructures.Cores.DataStore;
using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using LYQ.Core.Infrastructures.Views.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace LYQ.Core.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IEntity, new()
    {
        protected IDataAccessor _dataAccessor;


        public virtual IEnumerable<TEntity> GetAll(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text)
        {
            return _dataAccessor.DbContext.Query<TEntity>(sql, param, transaction: dbTransaction == null ? _dataAccessor.DbTransaction : dbTransaction, commandType: commandType, commandTimeout: _dataAccessor.CommandTimeout);
        }

        public virtual TEntity GetById(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text)
        {
            return _dataAccessor.DbContext.QueryFirstOrDefault<TEntity>(sql, param, transaction: dbTransaction == null ? _dataAccessor.DbTransaction : dbTransaction, commandType: commandType, commandTimeout: _dataAccessor.CommandTimeout);
        }

        public PageResult<TEntity> GetPageList(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text)
        {
            var multiple = _dataAccessor.DbContext.QueryMultiple(sql, param, transaction: dbTransaction == null ? _dataAccessor.DbTransaction : dbTransaction, commandType: commandType, commandTimeout: _dataAccessor.CommandTimeout);

            var pageResult = new PageResult<TEntity>();

            pageResult.PageData = multiple.Read<TEntity>();

            pageResult.TotalCount = multiple.ReadFirst<int>();

            return pageResult;
        }

        public virtual int Insert(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text)
        {
            return _dataAccessor.DbContext.Execute(sql, param, dbTransaction == null ? _dataAccessor.DbTransaction : dbTransaction, commandTimeout: _dataAccessor.CommandTimeout, commandType: commandType);
        }

        public int Update(string sql, object param = null, IDbTransaction dbTransaction = null, CommandType commandType = CommandType.Text)
        {
            return _dataAccessor.DbContext.Execute(sql, param, dbTransaction == null ? _dataAccessor.DbTransaction : dbTransaction, commandTimeout: _dataAccessor.CommandTimeout, commandType: commandType);
        }
    }
}
