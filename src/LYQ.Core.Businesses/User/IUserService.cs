﻿using LYQ.Core.Businesses.User.Dto;
using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using System.Collections.Generic;

namespace LYQ.Core.Businesses.User
{
    public interface IUserService : IService
    {
        IList<UserDto> GetAllUser();

    }

}
