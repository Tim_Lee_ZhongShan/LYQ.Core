﻿using AutoMapper;
using LYQ.Core.Businesses.User.Dto;
using LYQ.Core.Repositories.User;
using System.Collections.Generic;
using System.Linq;

namespace LYQ.Core.Businesses.User
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public IList<UserDto> GetAllUser()
        {
            var entities = _userRepository.GetAllUser().ToList();
            
            return _mapper.Map<List<UserDto>>(entities);
        }
    }
}
