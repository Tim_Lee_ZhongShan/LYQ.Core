﻿using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using LYQ.Core.Infrastructures.Cores.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Businesses.User.Dto
{
    public class UserDto 
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }


    }
}
