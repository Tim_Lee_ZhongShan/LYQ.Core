﻿using AutoMapper;
using LYQ.Core.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Businesses.User.Dto
{
    public class UserDtoProfile : Profile
    {
        public UserDtoProfile() 
        {
            CreateMap<UserEntity, UserDto>().ReverseMap();
        }

    }
}
