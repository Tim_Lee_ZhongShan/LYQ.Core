﻿using LYQ.Core.Infrastructures.Cores.Injections.Dependencies;
using System;
using System.Collections.Generic;
using System.Text;

namespace LYQ.Core.Businesses
{
    public class BaseDto : IDto
    {
        public virtual string CreatedBy { get; set; }

        public virtual DateTime? CreatedDate { get; set; }

        public virtual string UpdatedBy { get; set; }

        public virtual DateTime? UpdatedDate { get; set; }

    }
}
